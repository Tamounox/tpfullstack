import { Component, OnInit } from "@angular/core";
import { DatePicker } from "@ionic-native/date-picker/ngx";

@Component({
  selector: "app-gender",
  templateUrl: "./gender.component.html",
  styleUrls: ["./gender.component.scss"]
})
export class GenderComponent implements OnInit {
  gender = "";
  besoins = [];
  rdv = "";
  heure = "";
  res = {};

  constructor(
    private datePicker: DatePicker
    ) {}

  ngOnInit() {}

  getGender(gender) {
    this.gender = gender;
    console.log(this.gender);
  }
  
  getBesoin(besoin) {
    this.besoins.push(besoin);
    console.log(this.besoins);
  }

  getRdv(rdv) {
    this.rdv = rdv;
    console.log(this.rdv);
  }

  getHeure(heure) {
    this.heure = heure;
    console.log(this.heure);
    this.reservation();
  }

  reservation() {
    this.res = {
      gender: this.gender,
      besoin: this.besoins,
      rdv: this.rdv,
      heure: this.heure
    };

    console.log(this.res);
  }

  displayDatePicker() {
    this.datePicker
      .show({
        date: new Date(),
        mode: "date",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      })
      .then(
        date => console.log("Got date: ", date),
        err => console.log("Error occurred while getting date: ", err)
      );
  }
}
